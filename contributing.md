The Ruff use in this project.

Installation
pip install ruff

Usage: ruff [OPTIONS] <COMMAND>
Commands:
- check    Run Ruff on the given files or directories (default)
- rule     Explain a rule (or all rules)
- config   List or describe the available configuration options
- linter   List all supported upstream linters
- clean    Clear any caches in the current directory and any subdirectories
- format   Run the Ruff formatter on the given files or directories
- server   Run the language server
- version  Display Ruff's version
- help     Print this message or the help of the given subcommand(s)

Options:
- -h, --help     Print help
- -V, --version  Print version
