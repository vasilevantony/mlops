Run project in a docker container:
1. Install Docker to your workstation if it hasn't: https://docs.docker.com/engine/install/
2. There are two variants of ENTRYPOINTs in Dockerfile:
    - v1 - run python script
    - v2 - run jupyter notebook
    Keep the row with target variant uncommented to build it.
3. Build an image: docker build . -t <image_name>.
4. Run image:
    - For v1: docker run <image_name>
    - For v2: docker run -p 8888:8888 -v <path_to_projects_folder>/notebooks:/app/notebooks --name test-notebook --rm -u 0 <image_name>
