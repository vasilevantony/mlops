FROM mambaorg/micromamba

WORKDIR /app

COPY environment.yml ./
RUN micromamba create -f environment.yml -y

COPY requirements.txt ./
RUN micromamba run -n dev pip install -r requirements.txt

COPY test.py ./
ENTRYPOINT [ "micromamba", "run", "-n", "dev", "python", "test.py" ]  # v1 - run python script
# ENTRYPOINT [ "micromamba", "run", "-n", "dev", "jupyter", "notebook", "--ip=0.0.0.0", "--port=8888", "--allow-root" ]  # v2 - run jupyter notebook
